//Custom eventListener - registers events and runs registered callbacks when any module emits corresponding event

var eventListener = (function() {
    var subscribers = [];

    //Add event listener
    function on(event, callback) {
        subscribers[event] = subscribers[event] || []; //Initialize new array if event type does not exist
        subscribers[event].push(callback);
    }

    //Execute registered event callbacks
    function emit(event) {
        if (!subscribers[event])
            return;

        subscribers[event].forEach(function(callback){
            callback();
        });


    }

    return {
        on: on,
        emit: emit
    }
})();