//Runs addEventListener on necessary dom objects
var domEventDelegate = (function(){
    _init();

    function _init(){
        generateCheckboxesListeners();

        //Delete button
        var delButton = document.getElementById("deleteButton");
        if(delButton)
            delButton.addEventListener("click",onDeleteButtonClick);

    }

    //Runs when checkbox is clicked, stores checked product sku in productProcessor module
     function onCheckBoxChange(event){
        checkBox = event.target;
         if(checkBox.checked){
            productProcessor.addCheckedProduct(checkBox.value);
             document.getElementById(checkBox.value).classList.remove("gradient-blue1");
            document.getElementById(checkBox.value).classList.add("gradient-red1");

         }else{
            productProcessor.removeCheckedProduct(checkBox.value);
             document.getElementById(checkBox.value).classList.remove("gradient-red1");
             document.getElementById(checkBox.value).classList.add("gradient-blue1");
         }
    }

    function onDeleteButtonClick(event){
        var text = "Delete " + productProcessor.getCheckedProducts().length + " products ?";
        alertBox.show(text,productProcessor.handleDeleterequest);

    }

    function generateCheckboxesListeners(){
        //Register events for checkbox change
        checkBoxes = document.querySelectorAll(".checkBoxInput");

        checkBoxes.forEach(function(item){
            item.addEventListener("change",onCheckBoxChange);
        }); //checkBoxes.forEach
    }
    return {
        generateCheckboxesListeners
    }
})();