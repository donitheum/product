var toastBox = (function(){
    var toastContainer = document.getElementById("toast");
    var toastMessage = document.getElementById("toastMessage");

    function show(message,color="green"){
        toastMessage.innerText = message;
        toastContainer.style.backgroundColor = color;
        toastContainer.style.display = "inline-block";
    }

    function hide(){
        toastContainer.style.display = "none";
    }

    return {
        show,
        hide
    }
})();