//deals with products submit form
var formProcessor =  (function(){
    var inputSKU = document.getElementById("inputSKU");
    var inputName = document.getElementById("inputName");
    var inputPrice = document.getElementById("inputPrice");
    var inputAttribute = document.getElementById("inputAttribute");
    var attributeText = document.getElementById("attributeText");
    var activeAttribute = Attribute.book;
    var submitButton = document.getElementById("submitForm");
    var attributeButtons = {
        [Attribute.book]: document.getElementById("buttonBook"),
        [Attribute.disc]: document.getElementById("buttonDisc"),
        [Attribute.furniture]: document.getElementById("buttonFurniture")
    };
    var selectedAttribute = Attribute.book;
    var attributeHint = document.getElementById("attributeHint");
    var toast = document.getElementById("toast");
    var toastMessage = document.getElementById("toastMessage");
    //furniture attributes
    var height = document.getElementById("height");
    var width = document.getElementById("width");
    var length = document.getElementById("length");
    var furnitureAttributesContainer = document.getElementById("furnitureAttributesContainer");
    var attributeContainer = document.getElementById("attributeContainer");
    _init();

    //initialize
    function _init(){
        setActiveAttributeButton();
        inputSKU.addEventListener('input',inputProcessor);
        inputName.addEventListener('input',inputProcessor);
        inputPrice.addEventListener('input',inputProcessor);
        inputAttribute.addEventListener('input',inputProcessor);
        height.addEventListener('input',inputProcessor);
        width.addEventListener('input',inputProcessor);
        length.addEventListener('input',inputProcessor);
        submitButton.addEventListener("click",sendFormData);

        for (let attributeButtonsKey in attributeButtons) {
            attributeButtons[attributeButtonsKey].addEventListener("click",changeAttributeType);
        }
        toast.addEventListener("animationend", function(){
            toast.style.display = "none";
            toast.classList.remove("fading");
        })
    }
    function validate(){

    }

    //Changes attribute type
    function changeAttributeType(event){
        switch(event.target.id){
            case "buttonBook":
                activeAttribute = Attribute.book;
                attributeHint.innerText = HintText.book;
                attributeContainer.style.display = "table-row";
                furnitureAttributesContainer.style.display = "none";
                inputProcessor(null); //reevaluate input fields

                break;
            case "buttonDisc":
                activeAttribute = Attribute.disc;
                attributeHint.innerText = HintText.disc;
                attributeContainer.style.display = "table-row";
                furnitureAttributesContainer.style.display = "none";
                inputProcessor(null); //reevaluate input fields
                break;
            case "buttonFurniture":
                activeAttribute = Attribute.furniture;
                attributeHint.innerText = HintText.furniture;
                attributeContainer.style.display = "none";
                furnitureAttributesContainer.style.display = "table-row";
                inputProcessor(null); //reevaluate input fields
                break;
        }
        setActiveAttributeButton();
    }

    //Marks the clicked attribute button as active
    function setActiveAttributeButton(){

        for (let attributeButtonsKey in attributeButtons) {
            attributeButtons[attributeButtonsKey].classList.remove("formActiveAttributeButton");
        }

        attributeButtons[activeAttribute].classList.add("formActiveAttributeButton");
    }


    //Send post request to api with form data
    function sendFormData(){
        xhr = new XMLHttpRequest();
        var url = "/products/products";

        xhr.onload = function(){
            var response = JSON.parse(xhr.responseText);
            var key = Object.keys(response)[0];
            showToast(key,response[key]);
            if(key == MessageType.success )
                clearInputs();
        };
        xhr.open("POST",url);
        xhr.send(generateJSON());
    }

    //Enables submit if input fields not empty
    function inputProcessor(event){

        if(fieldsNotEmpty())
            submitButton.disabled = false;
        else
            submitButton.disabled = true;
    }

    //returns true if input fields not empty
    function fieldsNotEmpty(){
        if(inputSKU.value.length > 0 && inputName.value.length > 0 && inputPrice.value.length > 0
            && ((activeAttribute != Attribute.furniture && inputAttribute.value.length > 0 )
            || (activeAttribute == Attribute.furniture && height.value.length > 0 && width.value.length > 0
                && length.value.length > 0))) return true;
        return false;
    }

    //generates json for post request
    function generateJSON(){
        var attributeValue;
        if(activeAttribute != Attribute.furniture)
            attributeValue = inputAttribute.value;
        else
            attributeValue = height.value + "x" + width.value + "x"+ length.value;
        var json = {
            "sku": inputSKU.value,
            "name":inputName.value,
            "price":inputPrice.value,
            "attribute":attributeValue,
            "type":activeAttribute
        };
        return JSON.stringify(json);
    }

    //TODO review possibility of restarting animation by replacing toast element with a clone
    function showToast(messageType,message){
        if(messageType === MessageType.error)
            toast.style.backgroundColor = "red";
        else
            toast.style.backgroundColor = "azure";
        toastMessage.innerText = message;
        void toast.offsetWidth;
        toast.classList.add("fading");
        toast.style.display = "inline-block";
    }
    function clearInputs(){
        inputAttribute.value = "";
        inputName.value = "";
        inputSKU.value = "";
        inputPrice.value = "";
        height.value = "";
        length.value = "";
        width.value = "";
    }
})();