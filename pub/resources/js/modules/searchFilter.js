
//Search filter module, reloads dom based on search filter results
var searchFilter = (function(){
    var filterInput = document.getElementById("filterInput");
    var filterType = document.getElementById("filterType");
    var template = document.getElementById("product-container-template").innerHTML;
    var compiledTemplate = Handlebars.compile(template);
    _init();

    function _init(){
        filterInput.addEventListener("input",applyFilter);
        filterType.addEventListener("change",function () {
            filterInput.value = "";
        });

    }

    //reload products based on filter

    //TODO better option could be just filtering DOM objects instead of making api request, but for this purpose is fine
    function applyFilter(){
        //When clearing filter load all products
        productProcessor.clearCheckedProducts();
        if(filterInput.value.length < 2)
        {

            var xhr = new XMLHttpRequest();
            xhr.onload = function() {
                var response = xhr.responseText;
                response = '{"result":' + xhr.responseText + "}"; //handlebars needs key for iteration

                document.getElementById("resultContainer").innerHTML = compiledTemplate(JSON.parse(response));
                domEventDelegate.generateCheckboxesListeners();
            };
            url ="/products/products";
            xhr.open("GET", url);
            xhr.send();
            return;
        }

        //Load products specified by filter
        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
            var response = xhr.responseText;
            response = '{"result":' + xhr.responseText + "}"; //handlebars needs key for iteration

            document.getElementById("resultContainer").innerHTML = compiledTemplate(JSON.parse(response));
            domEventDelegate.generateCheckboxesListeners();
        };
        url ="/products/products/" + filterType.value + "/" + filterInput.value;
        xhr.open("GET", url);
        xhr.send();
    }
})();