
//Handles confirmation box for confirming deletion of products
var alertBox = (function(){
    var box = document.getElementById("alertContainer");
    var yesButton = document.getElementById("alertYes");
    var cancelButton = document.getElementById("alertNo");
    var alertText = document.getElementById("alertText");
    var callback = null; // function to call after yes is pressed


    _init();

    function _init(){
        yesButton.addEventListener("click",processResponse);
        cancelButton.addEventListener("click",processResponse);
    }

    //parameters - Alert text, callback for yes button
    function show(text,_callback){
        alertText.innerText = text;
        callback = _callback;
        box.style.display = "block";
    }

    function hide(){
        box.style.display = "none";
    }

    function processResponse(event){
        if(event.target.value=="Yes")
            callback();
        hide();
    }
    return {
        show,
        hide
    }
})();