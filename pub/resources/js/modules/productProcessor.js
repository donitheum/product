// Handles adding, deleting products through ajax

var productProcessor = (function (){
    var checkedProducts = [];

    function addCheckedProduct(sku){
        checkedProducts.push(sku);
        var deleteButton = document.getElementById("deleteButton");
        if(deleteButton.disabled)
            deleteButton.disabled = false;

    }

    function removeCheckedProduct(sku){
        const index = checkedProducts.indexOf(sku);
        checkedProducts.splice(index,1);
        if(checkedProducts.length < 1){
            document.getElementById("deleteButton").disabled = true;
        }
    }


    function getCheckedProducts(){
        return checkedProducts;
    }

    function clearCheckedProducts(){
        checkedProducts = [];
        document.getElementById("deleteButton").disabled = true;
    }

    function handleDeleterequest(){
        var xhr = new XMLHttpRequest();

        xhr.onload = function(e){
            checkedProducts.forEach(function(item){
                document.getElementById(item).remove();
            });
            checkedProducts = [];
            document.getElementById("deleteButton").disabled = true;
        };

        var url ='/products/products';
        xhr.open('DELETE',url,true);

        var json = "[";
        checkedProducts.forEach(function(item){
           json += "{ \"sku\":\"" + item + "\"},"
        });

        json = json.replace(/(^[,\s]+)|([,\s]+$)/g, ''); // remove trailing comma
        json +="]";
        console.log(json);
        xhr.setRequestHeader("content-type","application/json");
        xhr.send(json);
    }
    return {
        addCheckedProduct,
        removeCheckedProduct,
        getCheckedProducts,
        handleDeleterequest,
        clearCheckedProducts
    }




})();