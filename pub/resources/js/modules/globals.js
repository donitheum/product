//Color enum
var Color;
var Attribute;
var MessageType;
var HintText;
(function (Color) {
    Color["productContainerPrimary"] = "#5386E4";
    Color["productContainerChecked"] = "red";
    Color["formAttributeButton"] = "#85ff52";
    Color["formAttributeButtonActive"] = "#185200";
})(Color || (Color = {}));


(function (Attribute) {
    Attribute["book"] = "Book";
    Attribute["disc"] = "Disc";
    Attribute["furniture"] = "Furniture";
})(Attribute || (Attribute = {}));

(function (MessageType){
    MessageType["error"] = "error";
    MessageType["success"] = "success";
})(MessageType || (MessageType = {}));

(function (HintText) {
    HintText["book"] = "Weight (g)";
    HintText["disc"] = "Size (MB)";
    HintText["furniture"] = "Dimensions (HxWxL)";
})(HintText || (HintText = {}));