<?php
namespace  Aigars;


use Aigars\App\Application;

use Aigars\App\Models\Book;
use Aigars\App\PrivateControllers\Database;
use Aigars\App\Config;
use PDO;
require "vendor/autoload.php";

Database::init(new PDO("sqlite:". Config::DB_NAME));


//$log = new App\Logger\Logger();
//$log->alert("This is alert message");
//$log->info("This is info message");
//$log->critical("This is critical log");
//$log->debug("This is debug message");
//$log->emergency("Catastrophic message");
//$log->error("oh my god theres an error");
//$log->notice("Yeah, would be nice to notice");
//$log->warning("first time it's just a warning");

$app = new Application();