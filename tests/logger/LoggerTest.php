<?php
use Aigars\App\Logger\Logger;
class LoggerTest extends \Psr\Log\Test\LoggerInterfaceTest
{
    public function getLogger()
    {
        return new Logger();
    }

    public function getLogs() : array
    {
        //When TEST_CASE_MODE active -  data is written to log.txt
        $filename = dirname(__DIR__,2);
        $filename .= "\\log.txt";
        $data = file_get_contents($filename);
        unlink($filename); //erase file every time contents are read

        $arr  = explode(PHP_EOL,$data);
        array_pop($arr);
        $result = [];
        foreach($arr as $data) {
            $result[] = $data;
        }
        return $result;
    }
}