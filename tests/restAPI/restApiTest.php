<?php
use Aigars\App\Config;
use Aigars\App\Controllers\Products;
use Aigars\App\Logger\Logger;
use Aigars\App\Models\Book;
use Aigars\App\Models\HTTP\Request;
use Aigars\App\PrivateControllers\Database;
use PDO;
use PHPUnit\Framework\TestCase;

class restApiTest extends TestCase {


    public function initialize(){
        Database::init(new PDO("sqlite:". "TestDB123456x.db"));
        $db = Database::getDatabase();
        $sql=$db->prepare("DROP TABLE  products ");
        if(!is_bool($sql))
            $sql->execute();
        return $db;
    }
    public function testAddProduct(){
        $db = $this->initialize();

        //Drop tables otherwise product might already exist from previous test which yields error

        $book = new Book("1231231aa","bible","12","12");

       $request = $this->createMock(Request::class);
       $request->method("getData")->willReturn(json_encode($book->toArray()));


        $products = new Products();

        $expected = '{"success":"Product created","data":{"sku":"1231231aa","name":"bible","price":12,"type":"book","attribute":"12"}}';

        $this->expectOutputString($expected);
        $res = $products->addProduct($request);
    }

    public function testProductExists(){
        $book = new Book("1231231aa","bible","12","12");

        $request = $this->createMock(Request::class);
        $request->method("getData")->willReturn(json_encode($book->toArray()));


        $products = new Products();

        $expected = '{"error":" Product with SKU: 1231231aa already exists!"}';

        $this->expectOutputString($expected);
        $res = $products->addProduct($request);
    }

    public function testDeleteProduct(){
        $request = $this->createMock(Request::class);
        $products = new Products();
        $expected = '{"success":"1 products deleted !"}';
        $request->method("getData")->willReturn('{"sku":"1231231aa"}');
        $this->expectOutputString($expected);
        $products->deleteProduct($request);
    }


    public function testEmptyString(){
        $book = new Book("","","12","12");

        $request = $this->createMock(Request::class);
        $request->method("getData")->willReturn(json_encode($book->toArray()));


        $products = new Products();

        $expected = '{"error":"Values cannot be empty"}';

        $this->expectOutputString($expected);
        $res = $products->addProduct($request);
    }

    public function testInvalidFurnitureAttribute(){
        $furniture = new \Aigars\App\Models\Furniture("453453453","table 1","12","12x22");

        $request = $this->createMock(Request::class);
        $request->method("getData")->willReturn(json_encode($furniture->toArray()));


        $products = new Products();

        $expected = '{"error":"Invalid attribute value, use HxWxL, max 5 digits each"}';

        $this->expectOutputString($expected);
        $res = $products->addProduct($request);
    }

}