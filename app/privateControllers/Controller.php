<?php
namespace Aigars\App\PrivateControllers;

//Controller subclass for associating service with a controller, like products will need productService to communicate with database
abstract class Controller {
    protected $service;

    public function __construct($service)
    {
        $this->service = $service;
    }
}