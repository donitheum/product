<?php
namespace Aigars\App\PrivateControllers;
final class Database {
    private static  $connection = null;

    public static function init($connection){
        if(Database::$connection==null)
            Database::$connection = $connection;
    }

    public static function &getDatabase(){
        return Database::$connection;

    }
}