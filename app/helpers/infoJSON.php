<?php
namespace Aigars\App\Helpers;

//Helper class for sending outcome messages in Response
class InfoJSON{
    const ERROR = "error";
    const SUCCESS = "success";

    public static function create($type, $message,$data=""){

        $json = [ $type => $message];
        if($data != "")
            $json["data"] = $data;
        return json_encode($json);
    }

}