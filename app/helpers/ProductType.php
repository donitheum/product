<?php
namespace Aigars\App\Helpers;
//Possible product types
class ProductType
{
    const BOOK = "book";
    const DISC = "disc";
    const FURNITURE = "furniture";
    const NONE = "none";

   private static $reflection = null;
   public static function getAllTypes()
   {
       if(ProductType::$reflection == null){
           ProductType::$reflection = new \ReflectionClass(__CLASS__);
       }
       return ProductType::$reflection->getConstants();
   }
}