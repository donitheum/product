<?php
namespace Aigars\App\Controllers;
use Aigars\App\Logger\Logger;
use Aigars\App\Models\HTTP\HTMLResponse;
use Aigars\App\Models\HTTP\Request;
use Aigars\App\Models\HTTP\StatusCode;
use Aigars\App\Services\ProductService;


//Renders results and addProduct pages
class Renderer{
    private $productService;

    //Renders results page
    public function results(Request $request){
        $this->productService = new ProductService();
        $data = ["include"=>"results.php", "data" => $this->productService->getAllRecords()];
        HTMLResponse::create(StatusCode::HTTP_OK)->responseContent("index.php",$data)->send();
    }

    //Render add product page
    public function addProduct(Request $request){
        $log = new Logger();
        $log->debug("addproduct called");
        $data = ["include"=>"addProduct.php"];
        HTMLResponse::create(StatusCode::HTTP_OK)->responseContent("index.php",$data)->send();
    }

}