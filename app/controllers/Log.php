<?php
namespace Aigars\App\Controllers;
use Aigars\app\Config;
use Aigars\App\Logger\Logger;
use Aigars\App\Models\HTTP\HTMLResponse;
use Aigars\App\Models\HTTP\Request;
use Aigars\App\Models\HTTP\StatusCode;
use Aigars\App\Services\ProductService;


//Log class accessing endpoint products/log will render log if Config.php LOG_ACTIVE is enabled
class Log extends \Aigars\App\PrivateControllers\Controller {

    public function __construct()
    {
        parent::__construct(new ProductService());
    }

    //Outputs log page if log is enabled in Config.php
    public function getLog(Request $request)
    {
        if (!Config::LOG_ACTIVE) {
            HTMLResponse::create(StatusCode::HTTP_NOT_FOUND)->responseContent("404.html")->send();
            return;
        }
        $path = dirname(__DIR__, 2);
        $data = @file_get_contents($path . "/log.txt");
        $json = [];
        if ($data) {
            $data = rtrim($data, "║");
            $data = explode("║", $data);

            foreach ($data as $log) {
                $log = explode("|", $log);
                $json[] = $log;
            }
        }

       HTMLResponse::create(StatusCode::HTTP_OK)->responseContent("log.php",$json)->send();
    }
}