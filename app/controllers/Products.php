<?php

/**
 * All controller methods recieve request as a parameter
 */
namespace Aigars\App\Controllers;
use Aigars\App\Helpers\HelperFunctions;
use Aigars\App\Helpers\InfoJSON;
use Aigars\App\Helpers\ProductType;
use Aigars\App\Logger\Logger;
use Aigars\App\Models\HTTP\JSONResponse;
use Aigars\App\Models\HTTP\Request;
use Aigars\App\Models\HTTP\Response;
use Aigars\App\Models\HTTP\ResponseContent;
use Aigars\App\Models\HTTP\ResponseType;
use Aigars\App\Models\HTTP\StatusCode;
use Aigars\App\PrivateControllers\Controller;
use Aigars\App\Services\ProductService;


/**
*REST api for Product class
**/


//TODO make exceptions for failures
final class Products extends Controller {

    public function __construct()
    {
        parent::__construct(new ProductService());
    }


    /* validate to check if post json contains correct key value pairs */
    function validateJSON($productJSON){
        //TODO implement function
        global $productService;
        $validationResult = [];

        $data = json_decode($productJSON);
        //prepare json response
        $response =JSONResponse::create(StatusCode::HTTP_NOT_ACCEPTABLE);
        //Check if all the Keys are present in the request
        if(count((array)$data) != 5 ||!property_exists($data,'type')
            || !property_exists($data,"name") || !property_exists($data,"price")
            || !property_exists($data,"attribute") || !property_exists( $data,"sku")){
            return $response->responseContent(InfoJSON::create(InfoJSON::ERROR,"JSON request must be comprised of fields sku,name,price,attribute,type"))->send();
        }

        //Check if any value is empty string
        foreach($data as $value){
            if(strlen($value) < 1){
                return $response->responseContent(InfoJSON::create(InfoJSON::ERROR,"Values cannot be empty"))->send();
            }
        }


        //Validate product Type
        $testPassed = false;
        $data->type = strtolower($data->type);
        foreach(array_values(ProductType::getAllTypes()) as $availableType){
            if($availableType == $data->type){
                $testPassed = true;
                break;
            }
        }
        if(!$testPassed){
            return $response->responseContent(InfoJSON::create(InfoJSON::ERROR,"Invalid product type"))->send();
        }

        //Integer of real numbers
        $regEx = "/^\d*\.?\d*$/";
        //Validate Price
        if(!preg_match($regEx,$data->price) || !(int)$data->price > 0){
            return $response->responseContent(InfoJSON::create(InfoJSON::ERROR,"Invalid price, please use only positive numbers"))->send();
        }

        //Validate type attributes
        switch($data->type){
            case ProductType::BOOK:
            case ProductType::DISC:
                if(!preg_match($regEx,$data->attribute) || !(int)$data->attribute > 0){
                    return $response->responseContent(InfoJSON::create(InfoJSON::ERROR,"Invalid attribute, please use only positive numbers"))->send();
                }
                break;
            case ProductType::FURNITURE:
                $regEx = "/^\\b\\d{1,5}x\\d{1,5}x\\d{1,5}\\b$/";
                if(!preg_match($regEx,$data->attribute))
                    return $response->responseContent(InfoJSON::create(InfoJSON::ERROR,"Invalid attribute value, use HxWxL, max 5 digits each"))->send();
                break;
        }



        //Check if product with the same SKU already exists
        if($this->service->productExists($data->sku)) {
            $messageBody = " Product with SKU: " . $data->sku . " already exists!";
            return $response->responseContent(InfoJSON::create(InfoJSON::ERROR,"$messageBody"))->send();
        }
        return 1;
    }


    public function addProduct(Request $request)
    {
        $response = JSONResponse::create();
        $json = $request->getData();

        //If invalid json
        if(!HelperFunctions::isJson($json))
                return $response->statusCode(StatusCode::HTTP_NOT_ACCEPTABLE)
                    ->responseContent(InfoJSON::create(InfoJSON::ERROR,"Invalid JSON request"))->send();


        //validate
       if($this->validateJSON($json) instanceof ResponseContent)
           return;

        $data = (array)json_decode($json);

        $result = $this->service->add($data);
        $response->statusCode(StatusCode::HTTP_CREATED)->responseContent(InfoJSON::create(InfoJSON::SUCCESS,"Product created",$result))->send();
    }

    public function getProducts(Request $request){
        $json = [];
        $i=0;
        foreach ($this->service->getAlLRecords() as $product){

            $json[$i] = $product->toArray();
            $i++;
        }
        JSONResponse::create(StatusCode::HTTP_OK)->responseContent($json)->send();

    }

    public function deleteProduct(Request $request){
        $response = JSONResponse::create();
        $json = $request->getData();
        if(!HelperFunctions::isJson($json))
            return $response->statusCode(StatusCode::HTTP_NOT_ACCEPTABLE)
                ->responseContent(InfoJSON::create(InfoJSON::ERROR,"Invalid JSON request"))->send();
        $data = json_decode($json);
        $itemsDeleted = $this->service->delete($data);
        if($itemsDeleted != -1 || $itemsDeleted == 0)
            return $response->statusCode(StatusCode::HTTP_OK)
                ->responseContent(InfoJSON::create(InfoJSON::SUCCESS,"$itemsDeleted products deleted !"))->send();
        else
            return $response->statusCode(StatusCode::HTTP_NOT_ACCEPTABLE)
                ->responseContent(InfoJSON::create(InfoJSON::ERROR,"Delete failed"))->send();

    }

    public function getProduct(Request $request){
         $data = $request->getArgumentList();
         JSONResponse::create(StatusCode::HTTP_OK)->responseContent($this->service->getproduct($data[0],$data[1]))->send();
    }
}