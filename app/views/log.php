<!--
    Page displays logs from log.txt
-->
<!DOCTYPE html>
<html>
<head>
    <title>Products app log</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="pub/resources/css/log.css">
</head>
<body>
    <table>
        <?php
        if($data == []){
            echo "Nothing in log file";
        }else {
            foreach ($data as $log) {
                $logClass = '';
                if (isset($log[1]))
                    $logClass = 'class="' . $log[1] . '"';
                echo "<tr $logClass>";
                for ($i = 0; $i < 3; $i++) {
                    if (isset($log[$i]))
                        $output = $log[$i];
                    else $output = " - ";
                    echo "<td>$output</td>";
                }
                echo "</tr>";
            }
        }
        ?>
    </table>
</body>
</html>