<div id="menuRight">
    <input type="button" id ="deleteButton" value="Delete" disabled> |
    <div id="filterMenu">
        <select id="filterType">
            <option value="sku">
                sku
            </option>
            <option value="name">
                name
            </option>
            <option value="type">
                type
            </option>
        </select>
        <input type="text" id="filterInput" placeholder="filter..">
    </div>
</div>
<div id="resultContainer" class="resultContainer">
<?php   foreach($data["data"] as $item) {
    $sku = $item->getSku();
    $name= $item->getName();
    $price = $item->getPrice();
    $attribute = $item->getAttribute();
    $type = $item->getType();
    $data = <<<TEMPLATE

<div class="product-container gradient-blue1" id = "$sku">
<div class="checkBoxDiv"><input type="checkbox" class="checkBoxInput" value="$sku"></div>

    <ul>
        <li><span class="title">SKU:</span> $sku</li>
        <li><span class="title">Name:</span> $name</li>
        <li><span class="title">Price:</span> $price</li>
        <li><span class="title">Type:</span> $type</li>
        <li><span class="title">Attrib:</span> $attribute</li>
    </ul>
</div>
TEMPLATE;
echo $data;
}


//        ?>
<script id="product-container-template" type="text/template">
    {{#each result}}
    <div class="product-container gradient-blue1" id = "{{sku}}">
        <div class="checkBoxDiv"><input type="checkbox" class="checkBoxInput" value="{{sku}}"></div>

        <ul>
            <li><span class="title">SKU:</span> {{sku}}</li>
            <li><span class="title">Name:</span> {{name}}</li>
            <li><span class="title">Price:</span> {{price}}</li>
            <li><span class="title">Type:</span> {{type}}</li>
            <li><span class="title">Attrib:</span> {{attribute}}</li>
        </ul>
    </div>
    {{/each}}
</script>
<script type="text/javascript" src="pub/resources/js/modules/searchFilter.js"></script>
</div>