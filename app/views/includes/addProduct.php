<div class="formContainer gradient-blue1">
    <table>
        <tr><td>Sku: </td><td><input id="inputSKU" type="text" autocomplete="off"></td></tr>
        <tr><td>Name:</td><td><input type="text" id="inputName" autocomplete="off"></td></tr>
        <tr><td>Price:</td><td><input type="number" min="0" id="inputPrice" autocomplete="off"></td></tr>
        <tr><td colspan="2" ><span id="buttonContainer"><input type="button" id="buttonBook" class="formAttributeButton" value="Book">
                <input type="button" id="buttonDisc" class="formAttributeButton" value="Disc">
                <input type="button" id="buttonFurniture" class="formAttributeButton" value="Furniture">
            </span></td></tr>
        <tr><td colspan="2"><span id="attributeHint">Weight (g)</span></td></tr>
        <tr id="attributeContainer"><td>Attribute:</td><td><input type="number" min="0" id="inputAttribute" autocomplete="off"></td></tr>
        <tr><td colspan="2">
        <table id="furnitureAttributesContainer"
            <tr ><td>Height:<input type="number" min="0" id="height" class="attributeInput" autocomplete="off"></td>
            <td>Width:<input type="number" min="0" id="width" class="attributeInput" autocomplete="off"></td></tr>
            <tr ><td colspan="2" style="text-align: center">Length:<input type="number" min="0" id="length" class="attributeInput" autocomplete="off"></td></tr>
        </table>
        </td></tr>
        <tr><td colspan="2">
                <input type="button" id="submitForm" value="submit" disabled>
            </td>
        </tr>
    </table>
</div>
<div id="toast" class="fading">
    <div id="toastMessage">Error: Invalid json file</div>
</div>
<script type="text/javascript" src="pub/resources/js/modules/toastBox.js"></script>
<script type="text/javascript" src="pub/resources/js/modules/formProcessor.js"></script>