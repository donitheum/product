<!doctype html>
<html lang= "en">

<head>
    <title>Hello Index</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="pub/resources/css/main.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.1/handlebars.min.js"></script>
    <script type="text/javascript" src="pub/resources/js/modules/globals.js"></script>
    <script type="text/javascript" src="pub/resources/js/modules/EventListener.js"></script>

</head>
<body>
    <header>
        <nav>
            <a href="/products">Home</a> |
            <a href="addproduct">Add</a>

        </nav>
    </header>

    <div id="mainContainer" class="mainContainer">
        <?php
            require_once(__dir__. "/includes/" .$data["include"]);
        ?>
    </div>

    <div id="alertContainer">
        <div id="alertBox">
            <div id="alertText">
                Are you sure want to delete 5 products ?
            </div>
            <div id="alertButtons">
                <input type="button" class="buttonType1" id="alertYes" value="Yes">
                <input type="button" class="buttonType1" id="alertNo" value="Cancel">
            </div>
        </div>
    </div>


    <script type="text/javascript" src="pub/resources/js/modules/alertBox.js"></script>
    <script type="text/javascript" src="pub/resources/js/modules/productProcessor.js"></script>
    <script type="text/javascript" src="pub/resources/js/modules/domEventDelegate.js"></script>
</body>

</html>