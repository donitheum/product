<?php
namespace Aigars\App\Services;
use Aigars\App\Helpers\HelperFunctions;
use Aigars\App\Logger\Logger;
use Aigars\App\Models\HTTP\Request;
use Aigars\App\Models\Product;
use Aigars\App\PrivateControllers\Database;
use PDO;

//Class acts as a middleware between Products controller and database records
class ProductService
{

    private $products = [];
    private $db = null; //Reference to database connection object
    public function __construct()
    {
        $this->db = Database::getDatabase();
    }


    //Retrieve all records from database store them in $products variable as Product objects.
    //Return all stored products
    public function getAllRecords(){
        $result = $this->db->query("SELECT * FROM products");
        if(is_bool($result))
            return [];
        foreach ($result->fetchAll(PDO::FETCH_ASSOC) as $item) {
            $this->products[] = Product::newInstance($item);
        }
        return $this->products;
    }



    //Create new Product object from json data save it to db and store in products array
    public function add(array $data){

    $product = Product::newInstance($data);
    $products[] = $product->save();
    return $product->toArray();

}



    //Delete all products with passed skus
    public function delete($data){

        //Prepare statement query
        $SKUs = [];
        if(is_array($data)){
            foreach ($data as $product){
                $SKUs[] = $product->sku;
            }
        }else{
            $SKUs[] = $data->sku;
        }

        $SKUs = array_unique($SKUs);
        $statementQuery = "DELETE FROM products WHERE sku in ( ";
        for($i=0;$i<count($SKUs);$i++){
            $statementQuery .= "?";
            if($i != count($SKUs) - 1)
                $statementQuery .= ",";
            else
                $statementQuery .= ");";
        }

        $stm = $this->db->prepare($statementQuery);
        for($i=0;$i<count($SKUs);$i++){
            $stm->bindParam($i+1,$SKUs[$i]);
        }
        try {
            if(is_bool($stm))
                return -1;
            $result = $stm->execute();
        }catch(Exception $e){
            return -1;
        }
        return $stm->rowCount();
    }



    //Get product from database where $property = $value
    //Todo possible sql injection with $property, refactor later
    public function getProduct($property,$value){
        if (!property_exists(Product::class,$property))
            return false;

        //$property = $this->db->quote($property);
        $query = "SELECT * FROM products WHERE $property LIKE :value";

        $stmnt = $this->db->prepare($query);
        if(is_bool($stmnt))
            return false;
        $value = $value . "%";
        $stmnt->bindParam(":value",$value);
        $stmnt->execute();
        $result  = $stmnt->fetchAll(PDO::FETCH_ASSOC);
        $data = [];
        foreach($result as $item){
            $data[] = $item;
        }
        return json_encode($data);
    }

    //Return all product values from database
    public function getProducts(){
        return $this->products;
    }

    public function productExists($SKU){
        $stm = $this->db->prepare("SELECT COUNT(*)  FROM products where sku=:SKU");
        if(is_bool($stm))
            return false;
        $stm->bindParam(":SKU",$SKU );
        $stm->execute();
        if($stm->fetchColumn() > 0)
            return true;
        return false;
    }
}