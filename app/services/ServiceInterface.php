<?php
interface ServiceInterface
{
    public function getAll();
    public function update();
    public function insert();
}