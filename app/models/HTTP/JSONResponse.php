<?php
namespace Aigars\App\Models\HTTP;
use Aigars\App\Helpers\HelperFunctions;
use Aigars\App\Logger\Logger;


//For sending json responses
class JSONResponse extends Response{
    protected function setResponse(ResponseContent $content, $statusCode){
        if(!headers_sent())
            header("Content-Type: application/json");
        if(HelperFunctions::isJson($content->getView()))
            $result = $content->getView();
        else $result = json_encode($content->getView());
        echo $result;
    }
}