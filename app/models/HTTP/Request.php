<?php
namespace Aigars\App\Models\HTTP;


use Aigars\App\Logger\Logger;
//Request class is passed to every mapped function described in app\Mapper.php
class Request extends  HTTP {

    private $method;
    public function __construct()
    {
        $this->headers = getallheaders();
        $this->data = file_get_contents('php://input');
        $this->method = $_SERVER["REQUEST_METHOD"];
    }



    public function getMethod(){
        return $this->method;
    }

}