<?php
namespace Aigars\App\Models\HTTP;


/*
 * Example of generating a response
 *      Response::create($statusCode)->responseContent($view,$data)->send();
 * calling this will pass $view and $statusCode to setResponse() function which needs to be implemented by subclass
 * contents and headers should be generated manually in forementioned function. Refer to JSONResponse for example.
 */
abstract class Response extends HTTP {

    protected $statusCode;

    public function __construct($statusCode = 200)
    {
        $this->statusCode = $statusCode;
        $this->content = new ResponseContent("");
    }

    abstract protected  function setResponse(ResponseContent $content, $statusCode);

     function responseContent($view, $data =[]){
         $this->data = new ResponseContent($view,$data);
         return $this;
     }

     function statusCode($statusCode){
         $this->statusCode = $statusCode;
         return $this;
     }
     function send(){
         http_response_code($this->statusCode);
         $this->setResponse($this->data,$this->statusCode);
         return $this->data;
     }


     //Return object instance "subClass"
     static function create(){
         $class  = static::class;
         return new $class();
     }

}