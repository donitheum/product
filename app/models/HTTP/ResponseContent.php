<?php
namespace Aigars\App\Models\HTTP;

/*
 * Response needs ResponseContent passed to its constructor
 */
class ResponseContent {
    private  $view; //View to be rendered
    private array $data; //Auxiliary data

    public function __construct($view,$data = []){
        $this->data = $data;
        $this->view = $view;
    }

    public function getView(){
        return $this->view;
    }
    public function setView($view){
        $this->view = $view;
    }

    public function getData(){
        return $this->data;
    }

    public function setData($data){
        $this->data = $data;
    }


}