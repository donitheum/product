<?php
namespace Aigars\App\Models\HTTP;
//parent class of Response
abstract class HTTP {
    protected $headers = [];
    protected $argumentList; //contains exploded path

    /**
     * @var data Implies request/response body, for Response it is an instance of ResponseBody.
     * for Request it is whatever is passed in POST request
     **/
    protected $data;

    public function getHeaders(){
        return $this->headers;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getArgumentList(){
        return $this->argumentList;
    }
    public function setArguments($arguments){
        $this->argumentList = $arguments;
    }
}