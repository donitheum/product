<?php
namespace Aigars\App\Models\HTTP;

use Aigars\App\views\PageRenderer;


// HTMLResponse::create(StatusCode::HTTP_OK)->responseContent("index.php")->send(); renders index.php
class HTMLResponse extends Response{
    protected function setResponse(ResponseContent $content, $statusCode)
    {
        header("Content-Type: text/html");
        PageRenderer::render($content->getView(),$content->getData());
    }

}