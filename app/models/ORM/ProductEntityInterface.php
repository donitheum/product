<?php
namespace Aigars\App\Models\ORM;
interface ProductEntityInterface
{
    public function save();
    public function createTableIfNotExists();
    public static function newInstance(array $data);
}