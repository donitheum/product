<?php
namespace Aigars\App\Models\ORM\Traits;
use Aigars\App\Models\Product;
use Aigars\App\PrivateControllers\Database;
use Aigars\App\Helpers\HelperFunctions;


//ORM for product class
trait TraitProductEntity {
    public $schema = [];
    private $id = 0;

    //Saves product to database
    public function save(){
        $this->createTableIfNotExists();
        $db = Database::getDatabase();
        $tableName = $this->schema["table"];
        $tableName .= "s";
        $ref = new \ReflectionClass($this);
        //$db = new \SQLite3();
        $exists = $db->query("SELECT COUNT(*) FROM $tableName WHERE id = $this->id ");
        $query = "";
        if(!$exists && $this->id != null) {
            $query = "UPDATE $tableName SET ";
            foreach ($this->schema["rows"] as $key => $value) {
                if($key=="id")
                    continue;
               $query .= "$key=" . $this->$key .",";
            }
            $query = rtrim($query,",");

            $query .= " WHERE id =  $this->id";
        }else {
            //TODO dont insert if the sku exists but with different id
            //TODO refactor it as a prepared statement
            $query = "INSERT INTO $tableName (";
            foreach ($this->schema["rows"] as $key=>$value){
                if($key == "id")
                    continue;
                $query .= "$key,";
            }
            $query = rtrim($query,",");
            $query .= ") VALUES (";
            foreach ($this->schema["rows"] as $key => $value){
                if($key == "id")
                    continue;
                if(!is_numeric($value))
                    $valueQuery  = "\"".$this->$key."\"";
                else $valueQuery = $this->$key;
                $query .= "$valueQuery,";
            }
            $query = rtrim($query,",");
            $query .= ")";
            $db->query($query);
            $this->id = $db->lastInsertID();

        }
        return $this;
    }


    /**
     * @throws \ReflectionException]
     * Extract class properties and their types from object, add them to schema array and
     * compose a query for creating database table with corresponding records
     */
    public function createTableIfNotExists(){

        if(sizeof($this->schema) > 0)
            return;

        $reflection = new \ReflectionClass(__CLASS__);
        $this->schema["rows"] = $reflection->getDefaultProperties();
        unset($this->schema["rows"]["schema"]);
        $className = $reflection->getShortName();
        $this->schema["table"] = $className;
        $schemaQueryString = "{$className}s(";
        $i = 0;
        foreach($this->schema["rows"] as $key=>$value){

            $type = "TEXT";
            if(is_numeric($value))
                $type = "REAL"; //TODO  if other types apart double and text necessary, just rewrite the function
            if($key=="id")
                $type = "INTEGER PRIMARY KEY AUTOINCREMENT";
            $schemaQueryString .= "$key $type,";
            $i++;
        }
        $schemaQueryString = rtrim($schemaQueryString,",");
        $schemaQueryString .= ")";
        $createTableString = <<<EOD
        CREATE TABLE IF NOT EXISTS 
            $schemaQueryString
        EOD;
        Database::getDatabase()->exec($createTableString);
    }

    private function set($property,$value){
        $this->$property = $value;
    }
    public static function &newInstance($data){
        $product = new Product();
        foreach ($data as $key=>$value){

            $product->set($key,$value);
        }
        return $product;
    }

    public function toArray($includeId = false){
        $properties = get_object_vars($this);
        unset($properties["schema"]);
        if(!$includeId)
            unset($properties["id"]);

        return  $properties;

    }

    public function getId(){
        return $this->id;
    }
}