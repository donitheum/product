<?php
namespace Aigars\App\Models;
use Aigars\App\Helpers\ProductType;

final class Furniture extends Product{
    /**
     * Furniture constructor.
     * @param string $sku
     * @param string $name
     * @param float $price
     */
    public function __construct(string $sku,string  $name,float $price,string $attribute)
    {
        parent::__construct($sku, $name, $price,$attribute, ProductType::FURNITURE);
    }
}