<?php
namespace Aigars\App\Models;

use Aigars\App\Helpers\ProductType;
use Aigars\App\Models\ORM\ProductEntityInterface;
use Aigars\App\Models\ORM\Traits\TraitProductEntity;
use phpDocumentor\Reflection\DocBlock\ExampleFinder;

class Product implements ProductEntityInterface
{

    use TraitProductEntity;

    /**
     * @var string
     */
    protected $sku = "text";
    /**
     * @var string
     */
    protected $name = "text";
    /**
     * @var float
     */
    protected $price = 0.0;
    /**
     * @var mixed
     */
    protected $type = "text";

    protected $attribute = "text";


    /**
     * Product constructor.
     * @param $sku - product sku
     * @param $name - product name
     * @param $price - product price
     * @param $type - product type
     * @param $attribute - product attribute value
     * **/
    public function __construct($sku="000", $name="empty", $price=0, $attribute="0", $type = ProductType::NONE)
    {
        //TODO if no sku given assign random sku, make sure db does not already contain the same
        $this->name = $name;
        $this->price = $price;
        $this->sku = $sku;
        $this->attribute = $attribute;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getSku() : string
    {
        return $this->sku;
    }

    /**
     * @return float
     */
    public function getPrice() : float
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getName() : string {
        return $this->name;
    }

    /**
     * @param string $sku
     */
    public function setSku(string $sku): void
    {
        $this->sku = $sku;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param mixed $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getAttribute(): string
    {
        return $this->attribute;
    }

    /**
     * @param string $attribute
     */
    public function setAttribute(string $attribute): void
    {
        $this->attribute = $attribute;
    }


}