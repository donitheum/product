<?php
namespace Aigars\App\Models;
use Aigars\App\Helpers\ProductType;

final class Disc extends Product{
    /**
     * Disc constructor.
     * @param string $sku
     * @param string $name
     * @param string $price
     */
    public function __construct(string $sku,string  $name,float $price,string $attribute)
    {
        parent::__construct($sku, $name, $price,$attribute, ProductType::DISC);
    }
}