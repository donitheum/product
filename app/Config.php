<?php
namespace Aigars\App;
class Config
{
    const DB_NAME = "products.db";
    const TEST_DB_NAME="testProducts.db";
    const LOG_ACTIVE = true;
}