<?php
namespace Aigars\App;
use Aigars\App\Controllers\Database;
use Aigars\App\Mapper;
use Aigars\App\Models\HTTP\HTMLResponse;
use Aigars\App\Models\HTTP\Request;
use Aigars\App\Models\HTTP\StatusCode;


//Bootstraps application together
class Application
{
    //TODO if database not initialized throw Exception
    private  $url;
    private Request $request;
    private $controller = null;


    public function __construct()
    {
        //Extract requested url
        $url = isset($_GET["url"]) ? $_GET["url"] : "";
        $this->url = rtrim($url,"/");
        $this->url = explode("/",$url);

        //Create Request object
        $this->request = new \Aigars\App\Models\HTTP\Request();
        //Map the request url to an object
        $this->mapRequest();
    }

    public function getRequest(){
        return $this->request;
    }

    /*
     * Maps request method and requested controller to corresponding class and method
     * Execute the class method if it the request mapping exists
     */
    public function mapRequest(){

        if(!isset($this->url[0]))
            return;


        $mapping = strtoupper($this->request->getMethod()) . " " . $this->url[0];

        //Check if mapping exists
        if(!isset(Mapper::MAPPING[$mapping])){
            HTMLResponse::create(StatusCode::HTTP_NOT_FOUND)->responseContent("404.html")->send();
            return;
        }
        $fullClassName =  Mapper::MAPPING[$mapping]['0'];

        //Map arguments
        if(sizeof($this->url) > 1) {
            for ($i = 1; $i < sizeof($this->url); $i++) {
                $mapping .= "/{" . ($i - 1) . "}";
            }
        }

        //If mapping with arguments does not exist
        if(!isset(Mapper::MAPPING[$mapping])){
            HTMLResponse::create(StatusCode::HTTP_NOT_FOUND)->responseContent("404.html")->send();
            return;
        }


        $map = Mapper::MAPPING[$mapping];
         if(class_exists($map[0])) {
            $this->controller = new $fullClassName();

            if(!isset($map[1]))
                return;
            if(method_exists($this->controller,$map[1])) {

                //If Url path comprises  more than one part -  add arguments to request
                if(sizeof($this->url) > 1)
                {
                    unset($this->url[0]);
                    $this->request->setArguments(array_values($this->url));
                }
                $this->controller->{$map[1]}($this->request);
            }
        }

    }
}