<?php
namespace Aigars\App;
use Aigars\App\Controllers\Log;
use Aigars\App\Controllers\Products;
use Aigars\App\Controllers\Renderer;
use Aigars\App\Controllers\RESTApi;
use Aigars\App\Controllers\Test;

/**
 * Class Mapper
 * @package Aigars\App
 * Mapping  "Request method URL" => [Class, method]
 * Any class Added to controllers folder can be mapped, but class methods need to take in Request argument
 *
 */
class Mapper{
    const MAPPING = [
        "POST products" => [Products::class ,'addProduct'],
        "GET products" =>  [Products::class,'getProducts'],
        "DELETE products" => [Products::class,'deleteProduct'],
        "GET products/{0}/{1}" => [Products::class,'getProduct'],
        "GET " => [Renderer::class,'results'],
        "GET addproduct" => [Renderer::class,"addProduct"],
        "GET log" => [Log::class,"getLog"]
    ];
}