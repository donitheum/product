<?php

namespace Aigars\App\Logger;

//Uncomment when running phpunit test
//define("TEST_CASE_MODE",true);
use Aigars\App\Config;

/**
 * Class Logger
 * Custom Logger class that adheres to PSR-3 Standard
 */


class Logger extends \Psr\Log\AbstractLogger
{


    private $logLevels = ['debug', 'info', 'notice', 'warning', 'error', 'critical', 'alert', 'emergency'];

    /**
     * @inheritDoc
     */
    public function log($level, $message, array $context = array()): void
    {

        //If invalid level throw exception
        if(!in_array($level,$this->logLevels)){
            throw new \Psr\Log\InvalidArgumentException();
        }

        //If message is an object call it's __toString();
        //Else try to interpolate it
        if(is_object($message)) {
            if (method_exists($message, '__toString'))
                $message = $message->__toString();
        }

        if(!empty($context))
            $message = $this->interpolate($message,$context);

        if(defined("TEST_CASE_MODE"))
        {
            //just for testcase
            $content = "$level $message" . PHP_EOL;
            file_put_contents("log.txt", $content,FILE_APPEND);
        }else{
            //log if logging enabled
            if(!Config::LOG_ACTIVE)
                return;
            if(is_array($message))
                $message = json_encode($message);
            $date =  date("Y-m-d, h:i:sa");
            $content = "$date|$level|$message ║";
            file_put_contents("log.txt", $content,FILE_APPEND);
        }

    }

    /**
     * replace message paceholders with correspnding array values
     * if value is an exception check that the psr-3 rules are followed
     * seems robust and straightforward enough.
     */
    private function interpolate($message, array $context = array()): string
    {
        // build a replacement array with braces around the context keys
        $replace = array();
        foreach ($context as $key => $val) {
            // check that the value can be casted to string
            if (!is_array($val) && (!is_object($val) || method_exists($val, '__toString'))) {
                //Before interpolating check psr-3 rules
                $doInterpolation = true;

                //If Exception passed - key has to be 'exception'
                //'exception' key MUST only contain Exception value
                if($val  instanceof \Exception)
                {
                    if($key != "exception")
                        $doInterpolation = false;
                }else if($key == "exception")
                    $doInterpolation = false;


                if($doInterpolation)
                    $replace['{' . $key . '}'] = $val;
            }
        }
        // interpolate replacement values into the message and return
        return strtr($message, $replace);
    }

    public function getLogLevels(): array {
        return $this->logLevels;
    }

}